export class Article {
    id;
    title;
    content;
    categorie;

    constructor (title, content, categorie, id = null) {
        this.title = title;
        this.content = content;
        this.categorie = categorie;
        this.id = id;
    }
}