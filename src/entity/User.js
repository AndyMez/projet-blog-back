export class User {
    id;
    username;
    email;
    password;
    role;

    constructor (username, email, password, role = 'user', id = null) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.id = id;
    }
}
