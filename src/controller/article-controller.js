import { Router } from "express";
import { articleRepository } from "../repository/ArticleRepository";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from "passport";

export const blogController = Router();

// ALL
blogController.get('/', async (req, res) => {
    try {
        let result = await new articleRepository().findAll();
        res.json(result)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

// ADD
blogController.post('/', passport.authenticate('jwt', {session:false}), async (req, res) => {
    try {
        console.log(req.body);
        let id = await new articleRepository().add(req.body)
        res.status(201).json(req.body)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

// DELETE
blogController.delete('/:id', passport.authenticate('jwt', {session:false}), async (req, res) => {
    try {
        await new articleRepository().delete(req.params.id);
        res.status(204)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

// UPDATE
blogController.patch('/', passport.authenticate('jwt', {session:false}), async (req, res) => {
    try {
        await new articleRepository().update(req.body);
        res.end();
        res.status(204)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

// FINDBYID
blogController.get('/:id', async (req, res) => {
    try {
        let result = await new articleRepository().findById(req.params.id)
        res.json(result)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})