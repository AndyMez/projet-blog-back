import { Router } from "express";
import { User } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from "passport";



export const userController = Router()

userController.post('/', async (req, res) => {
    try {
        const newUser = new User();
        Object.assign(newUser, req.body);
        
        const exists = await UserRepository.findByEmail(newUser.email);
        if(exists) {
            res.status(400).json({error: 'Email already taken'});
            return;
        }
        
        newUser.role = 'user';
        
        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.add(newUser);
        res.status(201).json(newUser);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userController.post('/login', async (req, res) => {
    try {
        const user = await UserRepository.findByEmail(req.body.email);
        if(user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        username: user.username,
                        email: user.email,
                        id:user.id,
                        role:user.role
                    })
                });
                return;
            }
        }
        res.status(401).json({error: 'Wrong email and/or password'});
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

userController.get('/account', passport.authenticate('jwt', {session:false}), (req,res) => {
    res.json(req.user);
});