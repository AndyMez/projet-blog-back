import { Article } from "../entity/Article";
import { connection } from './connection';


export class articleRepository {

    // ALL
    async findAll() {
        const [rows] = await connection.execute('SELECT * FROM article');
        const article = [];
        for (const row of rows) {
            let instance = new Article(row.title, row.content, row.categorie, row.id);
            article.push(instance);
        }
        return article 
    }

        // FINDBYID
        async findById(id) {
            const [rows] = await connection.execute('SELECT * FROM article WHERE id=?', [id])
            const article = [];
            for (const row of rows) {
                let instance = new Article(row.title, row.content, row.categorie, row.id)
                article.push(instance)
            }
            return article
        }

    // ADD
    async add(article) {
        const [result] = await connection.execute('INSERT INTO article (title, content, categorie) VALUES (?, ?, ?)', [article.title, article.content, article.categorie]);
        return article.id = result.insertId;
    }

    // DELETE
    async delete(id) {
        const [rows] = await connection.execute('DELETE FROM article WHERE id=?', [id])
    }

    // UPDATE
    async update(update) {
        const [rows] = await connection.execute('UPDATE article SET title=?, content=?, categorie=? WHERE id=?', [update.title, update.content, update.categorie, update.id])
    }
}

