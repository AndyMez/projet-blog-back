import { connection } from './connection';
import { User } from "../entity/User";

export class UserRepository {


    /**
     * @param {User} user 
     */
        static async add(user) {
            const [rows] = await connection.query('INSERT INTO user (username, email,password,role) VALUES (?, ?, ?, ?)', [user.username, user.email, user.password, user.role]);
            user.id= rows.insertId;
        }
        /**
         * @param {string} email 
         * @returns {Promise<User>}
         */
        static async findByEmail(email) {
            const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email]);
            if(rows.length === 1) {
                return new User(rows[0].username,rows[0].email, rows[0].password, rows[0].role, rows[0].id);
            }
            return null;
    
        }
    }