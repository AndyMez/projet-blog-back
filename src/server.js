import express from 'express';
import cors from 'cors';
import { blogController } from "./controller/article-controller";
import { configurePassport } from './utils/token';
import passport from 'passport';
import { userController } from './controller/user-controller';

configurePassport();

export const server = express();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());

server.use('/api/blog', blogController);
server.use('/api/user', userController);